import os 
import sys
import re
import time
import linecache
import globalt
import vulnscans
from clear import clear
from logo import *

def start(): 
    with open('nikto/proxy.py') as f:
        global proxy
        proxy = f.read()
    with open('nikto/customArgs.py') as f:
        global customArgs
        customArgs = f.read()
        
    global target
    target = globalt.target
    clear()
    menu()
    

def menu():
    logo()
    global proxy
    global target
    global customArgs
    print("Current Target: [" + BC.F + target + BC.G + "]")
    mitems("items here")
    mi = input("")
    mp = mi[:7]
    mo = mi[8:]
    np = mi[:5]
    no = mi[6:]